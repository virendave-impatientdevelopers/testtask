import React from "react";

export default () => (
  <div>
    <div style={{ textAlign: "center" }}>
      <h1 className="test mb-0">Get Started</h1>
    </div>
    <div style={{ backgroundImage: 'url("https://cdn.pixabay.com/photo/2016/09/22/10/44/banner-1686943_1280.jpg")', height: '45vh', backgroundSize: '100% 100%' }} className="w-100"></div>
    <section>
      <div className="jumbotron jumbotron-fluid m-0">
        <div className="container">
          <div className="mx-auto my-auto text-center">
            <h1>Section Header</h1>
            <p className="mx-auto">Section Body.</p>
          </div>
        </div>
      </div>
    </section>
    <section className="row" >
      <div className="col-sm-6 col-xs-12 jumbotron jumbotron-fluid m-0">
        <div className="container">
          <div className="mx-auto my-auto text-center">
            <h1>Section Header</h1>
            <p className="mx-auto">Section Body.</p>
          </div>
        </div>
      </div>
      <div className="col-sm-6 col-xs-12 p-0">
        <img src="https://cdn.pixabay.com/photo/2016/09/22/10/44/banner-1686943_1280.jpg" alt="Bg" className="w-100 h-100" />
      </div>
    </section>
  </div>
);
