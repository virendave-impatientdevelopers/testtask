import React from "react";

export default () => (
  <div className="col-md">
    <h3>Service Header</h3>
    <p>
      Sed turpis tincidunt id aliquet risus. Montes nascetur ridiculus mus
      mauris vitae ultricies leo. Posuere lorem ipsum dolor sit amet consectetur
      adipiscing. Lectus vestibulum mattis ullamcorper velit. Proin libero nunc
      consequat interdum varius sit amet. Non quam lacus suspendisse faucibus
      interdum posuere lorem ipsum dolor. Laoreet suspendisse interdum
      consectetur libero id faucibus nisl tincidunt eget.
    </p>
  </div>
);
